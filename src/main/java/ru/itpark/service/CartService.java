package ru.itpark.service;

import ru.itpark.dto.PurchaseAddDTO;
import ru.itpark.dto.PurchaseModifyDTO;
import ru.itpark.entity.Purchase;

import java.util.List;

public interface CartService {
    List<Purchase> findAllIncomplete(int clientId);

    void process(int clientId);

    void addFromDTO(int clientId, PurchaseAddDTO dto);

    void modifyFromDTO(int clientId, int purchaseId, PurchaseModifyDTO dto);

    void remove(int clientId, int purchaseId);
}
