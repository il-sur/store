package ru.itpark.service;

import org.springframework.stereotype.Service;
import ru.itpark.dto.PurchaseAddDTO;
import ru.itpark.dto.PurchaseModifyDTO;
import ru.itpark.entity.Client;
import ru.itpark.entity.Order;
import ru.itpark.entity.Product;
import ru.itpark.entity.Purchase;
import ru.itpark.exception.ClientNotFoundException;
import ru.itpark.exception.ProductNotFoundException;
import ru.itpark.exception.PurhcaseNotFoundException;
import ru.itpark.repository.ClientRepository;
import ru.itpark.repository.OrderRepository;
import ru.itpark.repository.ProductRepository;
import ru.itpark.repository.PurchaseRepository;

import java.util.Date;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    private final ClientRepository clientRepository;
    private final ProductRepository productRepository;
    private final PurchaseRepository purchaseRepository;
    private final OrderRepository orderRepository;

    public CartServiceImpl(ClientRepository clientRepository, ProductRepository productRepository, PurchaseRepository purchaseRepository, OrderRepository orderRepository) {
        this.clientRepository = clientRepository;
        this.productRepository = productRepository;
        this.purchaseRepository = purchaseRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Purchase> findAllIncomplete(int clientId) {
        return purchaseRepository.findAllByClientIdAndCompleteIsFalse(clientId);
    }

    @Override
    public void process(int clientId) {
        Client client = clientRepository.findById(clientId).orElseThrow(ClientNotFoundException::new);
        List<Purchase> purchases = purchaseRepository.findAllByClientIdAndCompleteIsFalse(clientId);

        Order order = new Order(new Date());
        order.setClient(client);
        order.setPurchases(purchases);
        orderRepository.save(order);

        purchases.forEach(e -> e.setComplete(true));
        purchaseRepository.saveAll(purchases);
    }

    @Override
    public void addFromDTO(int clientId, PurchaseAddDTO dto) {
        Client client = clientRepository.findById(clientId).orElseThrow(ClientNotFoundException::new);
        Product product = productRepository.findById(dto.getProductId()).orElseThrow(ProductNotFoundException::new);

        Purchase purchase = new Purchase(
                product.getPrice(),
                dto.getQuantity(),
                false
        );

        purchase.setClient(client);
        purchase.setProduct(product);

        purchaseRepository.save(purchase);
    }

    @Override
    public void modifyFromDTO(int clientId, int purchaseId, PurchaseModifyDTO dto) {
        Purchase purchase = purchaseRepository.findByIdAndClientId(purchaseId, clientId).orElseThrow(PurhcaseNotFoundException::new);

        purchase.setQuantity(dto.getQuantity());
        purchaseRepository.save(purchase);
    }


    @Override
    public void remove(int clientId, int purchaseId) {
        purchaseRepository.deleteByIdAndClientId(purchaseId, clientId);
    }
}


