package ru.itpark.service;

import ru.itpark.entity.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();

    List<Product> findByName(String name);
}
