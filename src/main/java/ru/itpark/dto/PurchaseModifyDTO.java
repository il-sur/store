package ru.itpark.dto;

import javax.validation.constraints.Min;

public class PurchaseModifyDTO {
    @Min(1)
    private int quantity;

    public PurchaseModifyDTO() {
    }

    public PurchaseModifyDTO(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
