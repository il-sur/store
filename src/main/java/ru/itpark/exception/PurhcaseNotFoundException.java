package ru.itpark.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PurhcaseNotFoundException extends RuntimeException{
    public PurhcaseNotFoundException() {
    }

    public PurhcaseNotFoundException(String message) {
        super(message);
    }

    public PurhcaseNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PurhcaseNotFoundException(Throwable cause) {
        super(cause);
    }

    public PurhcaseNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
