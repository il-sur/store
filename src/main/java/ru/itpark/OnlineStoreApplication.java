package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ru.itpark.entity.Client;
import ru.itpark.entity.Product;
import ru.itpark.repository.ClientRepository;
import ru.itpark.repository.ProductRepository;

import java.util.List;

@SpringBootApplication
public class OnlineStoreApplication {

//    public static void main(String[] args) {
//        SpringApplication.run(OnlineStoreApplication.class, args);
//    }
public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(OnlineStoreApplication.class, args);
    {
        ClientRepository repository = context.getBean(ClientRepository.class);
        repository.save(new Client("Vasya"));
    }

    {
        ProductRepository repository = context.getBean(ProductRepository.class);
        repository.saveAll(List.of(
                new Product("iPhone", 60_000),
                new Product("iPad", 40_000),
                new Product("Macbook", 120_000)
        ));
    }
}
}
