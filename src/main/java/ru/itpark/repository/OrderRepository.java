package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {
}
