package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itpark.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {
}
