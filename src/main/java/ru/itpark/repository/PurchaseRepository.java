package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itpark.entity.Client;
import ru.itpark.entity.Purchase;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    @Query("select p from Purchase p where p.client.id = :clientId")
    List<Purchase> findAllByClientIdAndCompleteIsFalse(@Param("clientId") int clientId);

    Optional<Purchase> findByIdAndClientId(int id, int clientId);

    @Modifying
    @Transactional
    @Query("delete from Purchase p where p.id = :id and p.client.id = :clientId")
    void deleteByIdAndClientId(@Param("id") int id, @Param("clientId") int clientId);
}
