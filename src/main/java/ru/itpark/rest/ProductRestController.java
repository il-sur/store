package ru.itpark.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.itpark.entity.Product;
import ru.itpark.service.ProductServiceImpl;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/products")
public class ProductRestController {
    private final ProductServiceImpl productService;

    public ProductRestController(ProductServiceImpl productService) {
        this.productService = productService;
    }

    @GetMapping
    @ApiOperation("Get all products")
    public List<Product> getAll() {
        return productService.findAll();
    }

    @GetMapping(path = "/searchByName", params = "name")
    @ApiOperation("Get product by name")
    public List<Product> findById(@RequestParam String name){
        return productService.findByName(name);
    }
}

