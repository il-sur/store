package ru.itpark.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.itpark.dto.PurchaseAddDTO;
import ru.itpark.dto.PurchaseModifyDTO;
import ru.itpark.entity.Purchase;
import ru.itpark.service.CartServiceImpl;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/clients/{clientId}/cart")
public class CartRestController {
    private final CartServiceImpl cartService;

    public CartRestController(CartServiceImpl cartService) {
        this.cartService = cartService;
    }

    @GetMapping
    @ApiOperation("Get incomplete purchases from cart")
    public List<Purchase> getAllIncomplete(@PathVariable int clientId) {
        return cartService.findAllIncomplete(clientId);
    }

    @PostMapping
    @ApiOperation("Add purchase to cart")
    public void add(@PathVariable int clientId, @RequestBody PurchaseAddDTO dto) {
        cartService.addFromDTO(clientId, dto);
    }

    @PutMapping
    @ApiOperation("Process order")
    public void process(@PathVariable int clientId) {
        cartService.process(clientId);
    }

    @PutMapping("/{purchaseId}")
    @ApiOperation("Modify purchase in cart")
    public void modify(@PathVariable int clientId, @PathVariable int purchaseId, @RequestBody PurchaseModifyDTO dto) {
        cartService.modifyFromDTO(clientId, purchaseId, dto);
    }

    @DeleteMapping("/{purchaseId}")
    @ApiOperation("Remove purchase from cart")
    public void modify(@PathVariable int clientId, @PathVariable int purchaseId) {
        cartService.remove(clientId, purchaseId);
    }
}


