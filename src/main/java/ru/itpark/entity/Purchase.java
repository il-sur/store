package ru.itpark.entity;

import javax.persistence.*;

@Entity
@Table(name = "purchases")
public class Purchase {
    @Id
    @GeneratedValue
    private int id;
    private int price;
    private int quantity;
    private boolean complete;

    @OneToOne
    private Product product;

    @ManyToOne
    private Client client;

    public Purchase() {
    }

    public Purchase(int id, int price, int quantity, boolean complete) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
        this.complete = complete;
    }

    public Purchase(int price, int quantity, boolean complete) {
        this.price = price;
        this.quantity = quantity;
        this.complete = complete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}